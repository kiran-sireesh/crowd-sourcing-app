
lazy val commonSettings = Seq(
  name := "crowd-sourcing-app",
  version := "1.0",
  scalaVersion := "2.11.8")

scalaVersion := "2.11.8"
//scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

lazy val libraries = {
  val akkaV = "2.4.10"
  val sprayV = "1.3.3"
  Seq(
    "io.spray" %% "spray-can" % sprayV,
    "io.spray" %% "spray-routing" % sprayV,
    "io.spray" %% "spray-json" % "1.3.2",
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-slf4j" % akkaV,
    "ch.qos.logback" % "logback-classic" % "1.1.7",
    "io.spray" %% "spray-client" % sprayV % "test",
    "io.spray" %% "spray-testkit" % sprayV % "test",
    "com.typesafe.akka" %% "akka-testkit" % akkaV % "test",
    "org.specs2" %% "specs2-core" % "2.5" % "test",
    "org.specs2" %% "specs2-mock" % "2.5" % "test",
    "org.scalatest" %% "scalatest" % "3.0.0" % "test",
    "info.cukes" %% "cucumber-scala" % "1.2.4" % "test"
  )
}

lazy val atlas = (project in file("."))
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= libraries)
  //.enablePlugins(CucumberPlugin)

lazy val app = (project in file("app")).
  settings(commonSettings: _*).
  settings(
    mainClass in assembly := Some("com.kiran.crowdfunding.CrowdFundingApp")
  )

lazy val utils = (project in file("utils")).
  settings(commonSettings: _*).
  settings(
    assemblyJarName in assembly := "crowd-sourcing-app.jar"
  )

enablePlugins(CucumberPlugin)

CucumberPlugin.glue := "com/kiran/crowdfunding/acceptance"


    