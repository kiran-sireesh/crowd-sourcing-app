Feature: Current Loan Offer

  Scenario Outline: Current Loan offer when there is no lender
    Given a borrower requested a loan of <money> for <number> days
    And there is no lender who has made an offer
    When borrower checks for current offers
    Then there are no current offers

    Examples:
      | money | number |
      | 1000  | 100    |
      | 1000  | 100    |
      | 500   | 1      |

  Scenario Outline: Current loan offer when there is just one lender who offered less than the requested loan amount
    Given a borrower requested a loan of <loan_amount> for <number> days
    And there is one lender who has made an offer to lend <offer_amount> with <interest_rate> (offer amount <= requested loan amount)
    When borrower checks for current offers
    Then the current offer amount and interest are <offer_amount> and <interest_rate> respectively i.e same as offered by lender

    Examples:
      | loan_amount | number | offer_amount | interest_rate |
      | 1000        | 100    | 720          | 5.3           |
      | 375         | 2      | 150          | 7.0           |
      | 1375        | 998    | 1375         | 3.0           |

  Scenario Outline: Current loan offer when there is just one lender who offered more than the requested loan amount
    Given a borrower requested a loan of <loan_amount> for <number> days
    And there is one lender who has made an offer to lend <offer_amount> with <interest_rate> (offer amount > requested loan amount)
    When borrower checks for current offers
    Then the current offer amount and interest are <loan_amount> and <interest_rate> respectively i.e amount capped at the requested loan amount

    Examples:
      | loan_amount | number | offer_amount | interest_rate |
      | 100         | 100    | 800          | 2.5           |
      | 75          | 2      | 150          | 7.0           |
      | 999         | 70     | 1000         | 5.9           |

  Scenario Outline: Current loan offer when there are lenders who combinedly offered less than the requested loan amount
    Given a borrower requested a loan of <loan_amount> for <number> days
    And there are lenders who have made offers to lend as given below (sum of all offers < requested loan amount)
      | amount | interest |
      | 100    | 5        |
      | 500    | 8.6      |
    When borrower checks for current offers
    Then the current offer amount and interest are 600 and 8 respectively (amount is sum of all offers and APR is combined lowest)

    Examples:
      | loan_amount | number |
      | 750         | 2      |
      | 1000        | 100    |
      | 99999       | 70     |

  Scenario Outline: Current loan offer when there are lenders who combinedly offered more than the requested loan amount
    Given a borrower requested a loan of <loan_amount> for <number> days
    And there are lenders who have made offers to lend as given below (sum of all offers > requested loan amount)
      | amount | interest |
      | 100    | 5        |
      | 600    | 6        |
      | 600    | 7        |
      | 5000   | 8.2      |
    When borrower checks for current offers
    Then the current offer amount and interest are <loan_amount> and <combined_interest_rate> respectively (amount is as requested, APR is combined lowest)

    Examples:
      | loan_amount | number | combined_interest_rate |
      | 1000        | 100    | 6.2                    |
      | 1800        | 70     | 6.89                   |
