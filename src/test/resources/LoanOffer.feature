Feature: Loan Offer

  Scenario Outline: Create Loan offer
    Given there is a loan request for <requested_amount> for <requested_days>
    When a lender creates an offer proposing to <lend_amount> at <interest_rate>
    Then a loan offer id is returned

    Examples:
      | requested_amount | requested_days | lend_amount | interest_rate |
      | 1000             | 100            | 100         | 5.5           |
      | 1000             | 100            | 500         | 8.6           |
      | 500              | 1              | 9000        | 15.4          |