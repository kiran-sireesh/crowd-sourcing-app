Feature: Loan Request

  Scenario Outline: Create loan request
    Given a borrower needs a loan
    When a request is placed to borrow <money> for <number> days
    Then a loan request-id is returned

    Examples:
      | money    | number |
      | 100      | 20     |
      | 23123    | 1      |
      | 12345678 | 300000 |