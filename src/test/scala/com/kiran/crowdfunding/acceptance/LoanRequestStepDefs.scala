package com.kiran.crowdfunding.acceptance

import com.kiran.crowdfunding.acceptance.HttpClient._
import com.kiran.crowdfunding.acceptance.World.map
import com.kiran.crowdfunding.domain.JsonFormatters._
import com.kiran.crowdfunding.domain.LoanRequestId
import cucumber.api.scala.{EN, ScalaDsl}
import org.scalatest.Matchers
import spray.client.pipelining._
import spray.http.MediaTypes._
import spray.http.StatusCodes.Created
import spray.http._
import spray.httpx.unmarshalling._

class LoanRequestStepDefs extends ScalaDsl with EN with Matchers {
  Given("""^a borrower needs a loan$""") { () =>
  }

  When("""^a request is placed to borrow (\d+) for (\d+) days$""") { (amount: Int, days: Int) =>
    val createLoanRequest = Post("/crowd-funding/loans",
      HttpEntity(`application/json`, s"""{"amount": $amount, "duration": $days}"""))

    map.put("create-loan-request", responseStatusAndBody(createLoanRequest))
  }

  Then("""^a loan request-id is returned$""") { () =>
    val createLoanResp = map("create-loan-request")

    createLoanResp.status shouldBe Created
    createLoanResp.entity.as[LoanRequestId] match {
      case Right(r) => {
        r.loanRequestId should not be empty
      }
      case Left(error) => fail(s"Error in unmarshalling to 'LoanRequestId' - $error")
    }
  }
}
