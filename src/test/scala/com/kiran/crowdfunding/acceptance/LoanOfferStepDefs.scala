package com.kiran.crowdfunding.acceptance

import com.kiran.crowdfunding.acceptance.HttpClient._
import com.kiran.crowdfunding.acceptance.World.map
import com.kiran.crowdfunding.domain.JsonFormatters._
import com.kiran.crowdfunding.domain.{LoanOfferId, LoanRequestId}
import cucumber.api.scala.{EN, ScalaDsl}
import org.scalatest.Matchers
import spray.client.pipelining._
import spray.http.MediaTypes._
import spray.http.StatusCodes.Created
import spray.http._
import spray.httpx.unmarshalling._

class LoanOfferStepDefs extends ScalaDsl with EN with Matchers {

  Given("""^there is a loan request for (\d+) for (\d+)$""") { (requestedAmount: Int, requestedDays: Int) =>
    val createLoanRequest = Post("/crowd-funding/loans",
      HttpEntity(`application/json`, s"""{"amount": $requestedAmount, "duration": $requestedDays}"""))

    val response = responseStatusAndBody(createLoanRequest)
    response.status shouldBe Created
    map.put("existing-loan-request", response)
  }

  When("""^a lender creates an offer proposing to (\d+) at (\d+\.?\d+)$""") { (lendAmount: Int, interestRateStr: String) =>
    val interestRate = interestRateStr.toFloat
    val loanRequestId = map("existing-loan-request").entity.as[LoanRequestId] match {
      case Right(r) => {
        r.loanRequestId should not be empty
        r.loanRequestId
      }
      case Left(error) => fail(s"Error in unmarshalling to 'LoanRequestId' - $error")
    }
    val createLoanOffer = Post(s"/crowd-funding/loans/$loanRequestId/offers",
      HttpEntity(`application/json`, s"""{"amount": $lendAmount, "interest": $interestRate}"""))

    val response = responseStatusAndBody(createLoanOffer)
    map.put("loan-offer", response)
  }

  Then("""^a loan offer id is returned$""") { () =>
    val statusAndBody = map("loan-offer")

    statusAndBody.status shouldBe Created
    statusAndBody.entity.as[LoanOfferId] match {
      case Right(r) => {
        r.loanOfferId should not be empty
      }
      case Left(error) => fail(s"Error in unmarshalling to 'LoanOfferId' - $error")
    }

  }
}
