package com.kiran.crowdfunding.acceptance

import java.util

import com.kiran.crowdfunding.acceptance.CurrentLoanOffersWorld.map
import com.kiran.crowdfunding.acceptance.HttpClient._
import com.kiran.crowdfunding.domain.JsonFormatters._
import com.kiran.crowdfunding.domain.{LoanOffer, LoanRequestId}
import cucumber.api.DataTable
import cucumber.api.scala.{EN, ScalaDsl}
import gherkin.formatter.model.DataTableRow
import org.scalatest.Matchers
import spray.client.pipelining._
import spray.http.MediaTypes._
import spray.http.StatusCodes.{Created, NotFound, OK}
import spray.http._
import spray.httpx.unmarshalling._

import scala.collection.JavaConversions._
import scala.collection.mutable

object CurrentLoanOffersWorld {
  val map = mutable.HashMap.empty[String, HttpStatusAndBody]
}

class CurrentLoanOfferStepDefs extends ScalaDsl with EN with Matchers {
  Given("""^a borrower requested a loan of (\d+) for (\d+) days$""") { (amount: Int, days: Int) =>
    val createLoanRequest = Post("/crowd-funding/loans",
      HttpEntity(`application/json`, s"""{"amount": $amount, "duration": $days}"""))

    val response = responseStatusAndBody(createLoanRequest)
    response.status shouldBe Created
    map.put("existing-loan-request", response)
  }

  Given("""^there is no lender who has made an offer$""") { () =>
  }

  When("""^borrower checks for current offers$""") { () =>
    val loanRequestId = getLoanRequestId("existing-loan-request")
    val response = responseStatusAndBody(Get(s"/crowd-funding/loans/$loanRequestId/current-offer"))
    map.put("current-loan-offer", response)
  }

  def getLoanRequestId(key: String) = {
    map(key).entity.as[LoanRequestId] match {
      case Right(r) => {
        r.loanRequestId should not be empty
        r.loanRequestId
      }
      case Left(error) => fail(s"Error in unmarshalling to 'LoanRequestId' - $error")
    }
  }

  Then("""^there are no current offers$""") { () =>
    map("current-loan-offer").status shouldBe NotFound
  }

  Given("""^there is one lender who has made an offer to lend (\d+) with (\d+\.?\d+) .*$""") {
    (offerAmount: Int, interestRateStr: String) =>
      val interestRate =
        createLoanOffer(getLoanRequestId("existing-loan-request"), LoanOffer(offerAmount, BigDecimal(interestRateStr)))
  }

  def createLoanOffer(loanRequestId: String, loanOffer: LoanOffer): Unit = {
    val createLoanOffer = Post(s"/crowd-funding/loans/$loanRequestId/offers",
      HttpEntity(`application/json`, s"""{"amount": ${loanOffer.amount}, "interest": ${loanOffer.interest}}"""))

    val response = responseStatusAndBody(createLoanOffer)
    response.status shouldBe Created
  }

  Then(
    """^the current offer amount and interest are (\d+) and (\d*\.?\d+) respectively.*$""") {
    (currentOfferAmount: Int, offerInterestRateStr: String) =>

      val offerInterestRate = offerInterestRateStr.toDouble
      val loanRequestId = getLoanRequestId("existing-loan-request")
      val curOfferRes = responseStatusAndBody(Get(s"/crowd-funding/loans/$loanRequestId/current-offer"))
      curOfferRes.status shouldBe OK

      curOfferRes.entity.as[LoanOffer] match {
        case Right(offer) => {
          offer.amount shouldEqual currentOfferAmount
          offer.interest shouldEqual offerInterestRate
        }
        case Left(error) => fail(s"Error in unmarshalling to 'LoanOffer' - $error")
      }
  }

  Given("""^there are lenders who have made offers to lend as given below.*$""") { (loanOffers: java.util.List[LoanOfferInput]) =>
    val loanRequestId = getLoanRequestId("existing-loan-request")
    loanOffers.toList.foreach(o => createLoanOffer(loanRequestId, LoanOffer(o.amount, BigDecimal(o.interest))))
  }

  case class LoanOfferInput(amount:Int, interest: Float)
}
