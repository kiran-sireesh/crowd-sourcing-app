package com.kiran.crowdfunding.acceptance

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.io.IO
import akka.pattern.ask
import com.kiran.crowdfunding.acceptance.HttpClient.HttpStatusAndBody
import com.kiran.crowdfunding.domain.JsonFormatters._
import com.kiran.crowdfunding.domain.LoanOffer
import spray.can.Http
import spray.client.pipelining._
import spray.http.{HttpResponse, _}

import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}

object HttpClient {
  implicit val system = ActorSystem()
  import system.dispatcher

  implicit val timeout = akka.util.Timeout(5, TimeUnit.SECONDS)
  val serverPort = sys.props.get("SERVER_PORT").getOrElse("8080").toInt

  private val pipeline: Future[SendReceive] =
    for (
      Http.HostConnectorInfo(connector, _) <-
      IO(Http) ? Http.HostConnectorSetup("localhost", port = serverPort)
    ) yield sendReceive(connector)

  def response(req: HttpRequest): HttpResponse = {
    Await.result(pipeline.flatMap(_ (req)), Duration(10, TimeUnit.SECONDS))
  }

  def responseStatusAndBody(req: HttpRequest): HttpStatusAndBody = {
    val httpResponse = Await.result(pipeline.flatMap(_ (req)), Duration(1, TimeUnit.SECONDS))
    HttpStatusAndBody(httpResponse.status, httpResponse.entity)
  }

  case class HttpStatusAndBody(status: StatusCode, entity: HttpEntity)

}

object World {
  val map = mutable.HashMap.empty[String, HttpStatusAndBody]
}