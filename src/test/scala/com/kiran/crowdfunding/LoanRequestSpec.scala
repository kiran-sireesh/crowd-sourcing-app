package com.kiran.crowdfunding

import com.kiran.crowdfunding.controller.LoanRequestController
import com.kiran.crowdfunding.db.CrowdFundingRepository
import com.kiran.crowdfunding.domain.JsonFormatters._
import com.kiran.crowdfunding.domain._
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification
import spray.http.HttpEntity
import spray.http.MediaTypes.`application/json`
import spray.http.StatusCodes.{BadRequest, Created}
import spray.testkit.Specs2RouteTest

class LoanRequestSpec extends Specification
  with Specs2RouteTest
  with LoanRequestController
  with Mockito {

  val crowdFundRepo = mock[CrowdFundingRepository]

  def actorRefFactory = system

  sequential

  "Create loan request" should {

    "successfully return a loan request id, given amount and duration" in {
      var requestId:LoanRequestId = null
      Post("/crowd-funding/loans",
        HttpEntity(`application/json`,
          """{"amount": 100, "duration": 50}""")) ~> sealRoute(loanRequestRoute) ~> check {
        status === Created
        requestId = responseAs[LoanRequestId]
      }
      there was one(crowdFundRepo).saveLoanRequest(SavedLoanRequest(requestId.loanRequestId, 100, 50))
    }

    "fail if amount is negative" in {
      var requestId = ""
      Post("/crowd-funding/loans",
        HttpEntity(`application/json`,
          """{"amount": -100, "duration": 50}""")) ~> sealRoute(loanRequestRoute) ~> check {
        status === BadRequest
      }
    }

    "fail if duration is negative" in {
      var requestId = ""
      Post("/crowd-funding/loans",
        HttpEntity(`application/json`,
          """{"amount": 100, "duration": -50}""")) ~> sealRoute(loanRequestRoute) ~> check {
        status === BadRequest
      }
    }
  }

}
