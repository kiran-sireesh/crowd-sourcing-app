package com.kiran.crowdfunding

import java.util.UUID

import com.kiran.crowdfunding.controller.LoanOfferController
import com.kiran.crowdfunding.db.CrowdFundingRepository
import com.kiran.crowdfunding.domain.JsonFormatters._
import com.kiran.crowdfunding.domain._
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification
import spray.http.HttpEntity
import spray.http.MediaTypes.`application/json`
import spray.http.StatusCodes.{BadRequest, Created, OK}
import spray.testkit.Specs2RouteTest

class LoanOfferSpec extends Specification
  with Specs2RouteTest
  with LoanOfferController
  with Mockito {

  def actorRefFactory = system

  val crowdFundRepo = mock[CrowdFundingRepository]

  sequential

  "Create loan offer" should {

    "return offer id, given proposed amount and interest for a loan request" in {
      val loanRequestId = UUID.randomUUID().toString
      crowdFundRepo.findLoanRequest(loanRequestId) returns
        Some(SavedLoanRequest(loanRequestId, 100, 20))

      var response:LoanOfferId = null
      Post(s"/crowd-funding/loans/$loanRequestId/offers" ,
        HttpEntity(`application/json`,
          """{"amount": 100, "interest": 7.5}""")) ~> sealRoute(loanOfferRoute) ~> check {
        status === Created
        response = responseAs[LoanOfferId]
      }
      there was one (crowdFundRepo).saveLoanOffer(SavedLoanOffer(response.loanOfferId, 100, 7.5f, loanRequestId))
    }

    "fail if loan request id is not present" in {
      val loanRequestId = UUID.randomUUID().toString
      crowdFundRepo.findLoanRequest(loanRequestId) returns None

      var offerId = ""
      Post(s"/crowd-funding/loans/$loanRequestId/offers" ,
        HttpEntity(`application/json`,
          """{"amount": 100, "interest": 7.5}""")) ~> sealRoute(loanOfferRoute) ~> check {
        status === BadRequest
      }
    }

    "fail if amount is negative" in {
      val loanRequestId = UUID.randomUUID().toString
      crowdFundRepo.findLoanRequest(loanRequestId) returns
        Some(SavedLoanRequest(loanRequestId, 100, 20))

      var offerId = ""
      Post(s"/crowd-funding/loans/$loanRequestId/offers" ,
        HttpEntity(`application/json`,
          """{"amount": -100, "interest": 7.5}""")) ~> sealRoute(loanOfferRoute) ~> check {
        status === BadRequest
      }
    }

    "fail if interest is negative" in {
      val loanRequestId = UUID.randomUUID().toString
      crowdFundRepo.findLoanRequest(loanRequestId) returns
        Some(SavedLoanRequest(loanRequestId, 100, 20))

      var offerId = ""
      Post(s"/crowd-funding/loans/$loanRequestId/offers" ,
        HttpEntity(`application/json`,
          """{"amount": 100, "interest": -1.23}""")) ~> sealRoute(loanOfferRoute) ~> check {
        status === BadRequest
      }
    }
  }

}
