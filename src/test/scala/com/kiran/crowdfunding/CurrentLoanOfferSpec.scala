package com.kiran.crowdfunding

import java.util.UUID

import com.kiran.crowdfunding.controller.CurrentLoanOfferController
import com.kiran.crowdfunding.db.CrowdFundingRepository
import com.kiran.crowdfunding.domain.JsonFormatters._
import com.kiran.crowdfunding.domain._
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification
import spray.http.StatusCodes.{BadRequest, OK}
import spray.testkit.Specs2RouteTest

class CurrentLoanOfferSpec extends Specification
  with Specs2RouteTest
  with CurrentLoanOfferController
  with Mockito {

  def actorRefFactory = system

  val crowdFundRepo = mock[CrowdFundingRepository]

  sequential

  "Get Current offer" should {

    "fail if loan request id is not present" in {
      val loanRequestId = UUID.randomUUID().toString
      crowdFundRepo.findLoanRequest(loanRequestId) returns None

      var offerId = ""
      Get(s"/crowd-funding/loans/$loanRequestId/current-offer") ~> sealRoute(currentLoanOfferRoute) ~> check {
        status === BadRequest
      }
    }

    "return current offer & lowest interest when total offer amount less than requested amount " in {
      val loanRequestId = UUID.randomUUID().toString
      crowdFundRepo.findLoanRequest(loanRequestId) returns
        Some(SavedLoanRequest(loanRequestId, 1000, 100))

      crowdFundRepo.findLoanOffers(loanRequestId) returns
        List(
          SavedLoanOffer("1", 100, 5, loanRequestId),
          SavedLoanOffer("2", 500, 8.6, loanRequestId))

      Get(s"/crowd-funding/loans/$loanRequestId/current-offer") ~> sealRoute(currentLoanOfferRoute) ~> check {
        status === OK
        responseAs[LoanOffer] === LoanOffer(600, 8)
      }
    }

    "return current offer & lowest interest when sum of all offers is more than requested amount" in {
      val loanRequestId = UUID.randomUUID().toString
      crowdFundRepo.findLoanRequest(loanRequestId) returns
        Some(SavedLoanRequest(loanRequestId, 1000, 100))

      crowdFundRepo.findLoanOffers(loanRequestId) returns
        List(
          SavedLoanOffer("1", 100, 5, loanRequestId),
          SavedLoanOffer("2", 600, 6, loanRequestId),
          SavedLoanOffer("3", 600, 7, loanRequestId),
          SavedLoanOffer("4", 500, 8.2, loanRequestId))

      Get(s"/crowd-funding/loans/$loanRequestId/current-offer") ~> sealRoute(currentLoanOfferRoute) ~> check {
        status === OK
        responseAs[LoanOffer] === LoanOffer(1000, 6.2)
      }

    }
  }

}
