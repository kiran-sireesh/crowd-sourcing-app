package com.kiran.crowdfunding

import com.kiran.crowdfunding.db.{CrowdFundingRepository, CrowdFundingRepositoryImpl}
import spray.routing.HttpServiceActor

class CrowdFundingActor extends HttpServiceActor with AllRoutes {

  val crowdFundRepo: CrowdFundingRepository = new CrowdFundingRepositoryImpl

  def receive = runRoute(allRoutes)
}
