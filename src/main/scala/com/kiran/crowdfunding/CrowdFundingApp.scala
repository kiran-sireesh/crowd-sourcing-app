package com.kiran.crowdfunding

import java.util.concurrent.TimeUnit

import akka.actor._
import akka.io.IO
import akka.util.Timeout
import spray.can.Http

object CrowdFundingApp extends App {

  val serverPort = sys.props.get("SERVER_PORT").getOrElse("8080").toInt

  implicit val system: ActorSystem = ActorSystem("crowd-funding")
  implicit val timeout = Timeout(5, TimeUnit.SECONDS)

  val service = system.actorOf(Props[CrowdFundingActor], "service-actor")

  IO(Http) ! Http.Bind(service, interface = "0.0.0.0", port = serverPort)
}
