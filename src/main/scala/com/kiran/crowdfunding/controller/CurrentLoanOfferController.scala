package com.kiran.crowdfunding.controller

import com.kiran.crowdfunding.db.CrowdFundingRepository
import com.kiran.crowdfunding.domain.JsonFormatters._
import com.kiran.crowdfunding.domain.{LoanOffer, SavedLoanOffer}
import spray.http.MediaTypes._
import spray.http.StatusCodes.{BadRequest, NotFound, OK}
import spray.routing.HttpService

trait CurrentLoanOfferController extends HttpService {

  val crowdFundRepo: CrowdFundingRepository

  val currentLoanOfferRoute =
    path("crowd-funding" / "loans" / Segment / "current-offer") { loanId =>
      get {
        val loanReq = crowdFundRepo.findLoanRequest(loanId)
        if (loanReq.isEmpty) {
          complete(BadRequest)
        }
        else {
          val offers = crowdFundRepo.findLoanOffers(loanId).sortBy(_.interest)
          if (offers.isEmpty) complete(NotFound)
          else {
            val loanOffer = offers.foldLeft(OfferComputation(0, 0.0))(
              (t: OfferComputation, offer: SavedLoanOffer) => {
                if (t.amount + offer.amount > loanReq.get.amount) {
                  val partialLoanAmount = loanReq.get.amount - t.amount
                  OfferComputation(loanReq.get.amount, t.aprWeightedAmount + (partialLoanAmount * offer.interest))
                }
                else {
                  OfferComputation(t.amount + offer.amount, t.aprWeightedAmount + (offer.amount * offer.interest))
                }
              }
            )

            respondWithMediaType(`application/json`) {
              val interestRate = (loanOffer.aprWeightedAmount / loanOffer.amount).setScale(2, BigDecimal.RoundingMode.HALF_UP)
              complete(OK, LoanOffer(loanOffer.amount, interestRate))
            }
          }
        }
      }
    }

  case class OfferComputation(amount: Int, aprWeightedAmount: BigDecimal)
}
