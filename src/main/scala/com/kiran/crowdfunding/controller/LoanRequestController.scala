package com.kiran.crowdfunding.controller

import java.util.UUID

import com.kiran.crowdfunding.db.CrowdFundingRepository
import com.kiran.crowdfunding.domain.JsonFormatters._
import com.kiran.crowdfunding.domain.{LoanRequest, LoanRequestId, SavedLoanRequest}
import spray.http.StatusCodes._
import spray.routing.HttpService

trait LoanRequestController extends HttpService {

  val crowdFundRepo: CrowdFundingRepository

  val loanRequestRoute =
    path("crowd-funding" / "loans") {
      post {
        entity(as[LoanRequest]) { l =>

          respondWithStatus(Created) {
            complete {
              val requestId = UUID.randomUUID().toString
              crowdFundRepo.saveLoanRequest(SavedLoanRequest(requestId, l.amount, l.duration))
              LoanRequestId(requestId)
            }
          }
        }
      }
    }

}
