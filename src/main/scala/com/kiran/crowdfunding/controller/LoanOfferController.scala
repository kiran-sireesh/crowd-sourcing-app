package com.kiran.crowdfunding.controller

import java.util.UUID

import com.kiran.crowdfunding.db.CrowdFundingRepository
import com.kiran.crowdfunding.domain.JsonFormatters._
import com.kiran.crowdfunding.domain.{LoanOffer, LoanOfferId, SavedLoanOffer}
import spray.http.StatusCodes._
import spray.routing.HttpService

trait LoanOfferController extends HttpService {

  val crowdFundRepo: CrowdFundingRepository

  val loanOfferRoute =
    path("crowd-funding" / "loans" / Segment / "offers") { loanId =>
      post {
        if (crowdFundRepo.findLoanRequest(loanId).isEmpty) {
          complete(BadRequest)
        }
        else {
          entity(as[LoanOffer]) { offer =>
            respondWithStatus(Created) {
              complete {
                val offerId = UUID.randomUUID().toString
                crowdFundRepo.saveLoanOffer(SavedLoanOffer(offerId, offer.amount, offer.interest, loanId))
                LoanOfferId(offerId)
              }
            }
          }
        }
      }
    }

}
