package com.kiran.crowdfunding.db

import com.kiran.crowdfunding.domain.{SavedLoanOffer, SavedLoanRequest}

trait CrowdFundingRepository {

  def saveLoanRequest(loan: SavedLoanRequest)

  def findLoanRequest(id: String): Option[SavedLoanRequest]

  def saveLoanOffer(offer: SavedLoanOffer)

  def findLoanOffers(loanRequestId: String): Seq[SavedLoanOffer]
}


