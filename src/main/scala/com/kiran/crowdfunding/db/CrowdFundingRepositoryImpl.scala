package com.kiran.crowdfunding.db

import com.kiran.crowdfunding.domain.{SavedLoanOffer, SavedLoanRequest}
import scala.collection.mutable

class CrowdFundingRepositoryImpl extends CrowdFundingRepository {

  val loanRequests = mutable.Set[SavedLoanRequest]()
  val loanOffers = mutable.Set[SavedLoanOffer]()

  def saveLoanRequest(loanReq: SavedLoanRequest): Unit = loanRequests += loanReq

  def findLoanRequest(id: String): Option[SavedLoanRequest] = loanRequests.find(_.id == id)

  def saveLoanOffer(offer: SavedLoanOffer): Unit = loanOffers += offer

  def findLoanOffers(loanRequestId: String): Seq[SavedLoanOffer] = loanOffers.filter(_.loanRequestId == loanRequestId).toSeq
}
