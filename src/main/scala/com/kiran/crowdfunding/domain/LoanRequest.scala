package com.kiran.crowdfunding.domain

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class LoanRequest(amount: Int, duration: Int) {
  require(amount >  0 && duration > 0, "'amount' and 'duration' must be positive integers")
}
case class SavedLoanRequest(id: String, amount: Int, duration: Int)
case class LoanRequestId(loanRequestId: String)

case class LoanOffer(amount: Int, interest: BigDecimal) {
  require(amount >  0 && interest > 0, "'amount' and 'interest' must be positive")
}
case class SavedLoanOffer(id:String, amount: Int, interest: BigDecimal, loanRequestId: String)
case class LoanOfferId(loanOfferId: String)

object JsonFormatters extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val loanRequestFormat = jsonFormat2(LoanRequest.apply)
  implicit val loanRequestIdFormat = jsonFormat1(LoanRequestId.apply)
  implicit val loanOfferFormat = jsonFormat2(LoanOffer.apply)
  implicit val loanOfferIdFormat = jsonFormat1(LoanOfferId.apply)
}