package com.kiran.crowdfunding

import com.kiran.crowdfunding.controller.{CurrentLoanOfferController, LoanOfferController, LoanRequestController}

trait AllRoutes extends LoanRequestController
  with LoanOfferController
  with CurrentLoanOfferController {
  //can add any common directives to handle CORS
  val allRoutes = loanRequestRoute ~ loanOfferRoute ~ currentLoanOfferRoute
}
