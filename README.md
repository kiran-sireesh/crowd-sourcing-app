# Crowd funding App #

App to borrow loans and lend money at your chosen interest rate.

### Run App

`sbt run -DSERVER_PORT=<PORT>`

Default port is 8080, if it's not specified.

### Tests

####End to end tests
* Run the app as mentioned above
* Run `sbt cucumber` to run end to end tests against the running app

Chose cucumber for writing acceptance tests as gherkin is easily understandable by product owners
and this should drive the unit tests and implementation.

####Unit tests
`sbt test`

Chose 'spray-testkit' for testing the endpoints.

### Endpoints and sample request/responses

#### Create loan request
**Request**:

>curl -X POST -H "Content-Type: application/json" -d '{"amount": 1000, "duration": 100}' "http://localhost:8080/crowd-funding/loans"

**Response**:
>{"loanRequestId": "22165995-4e18-41cf-8cdc-ef5e800fc9a3"}

#### Create loan offer
**Request**:

>curl -X POST -H "Content-Type: application/json" -d '{"amount": 500, "interest": 8.2}' "http://localhost:8080/crowd-funding/loans/22165995-4e18-41cf-8cdc-ef5e800fc9a3/offers"

**Response**:
>{"loanOfferId": "d38971ba-2ba3-4af2-b278-9dab2a0d7902"}


#### Get current offer
**Request**:

>curl -X GET -H "Content-Type: application/json" "http://localhost:8080/crowd-funding/loans/22165995-4e18-41cf-8cdc-ef5e800fc9a3/current-offer"

**Response**:

>{
  "amount": 1000,
  "interest": 6.199999809265137
}


### Reasons, justifications & assumptions
* Assumed that required loan amount falls within the `Int` range.
* Used `BigDecimal` for interest rate and calculating weighted minimun interest so that it's accurate.
  Rounded up this to 2 decimals as I think that's the common convention for representing interest rates
* Used sbt as the build tool because that's the most commonly used tool.
  